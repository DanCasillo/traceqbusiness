//
//  AddViewController.swift
//  TraceQGestore
//
//  Created by Simone Di Guida on 08/06/2020.
//  Copyright © 2020 Simone Di Guida. All rights reserved.
//

import UIKit
import Firebase

class AddCustomerViewController: UIViewController {
    
    @IBOutlet weak var addfield: UITextField!
    
    @IBOutlet weak var clientlabel: UILabel!
    @IBOutlet weak var plusbutton: UIButton!
    @IBOutlet weak var minusbutton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    var people: Int = 1 {
           didSet {
               if people == 1 {
                   self.minusbutton.alpha = 0.5
                   self.minusbutton.isUserInteractionEnabled = false
                   
                   
               } else {
                   self.minusbutton.alpha = 1
                   self.minusbutton.isUserInteractionEnabled = true
               }
           }
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        minusbutton.isEnabled = false
        minusbutton.alpha = 0.5
        
       
        
        
        clientlabel.layer.cornerRadius = 10
        clientlabel.layer.masksToBounds = true
        
        
        
        addfield.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func addclient(_ sender: Any) {
        let ref = Database.database().reference()
        let now = Date()
        let code = UserDefaults.standard.value(forKey: "codeActivity") as? String
     /*   ref.child("Prenotazione").child(code!).childByAutoId().setValue(["Nome" : addfield.text! as NSString, "Numero": Int(self.manyPeople.text!)! as NSNumber, "Orario" : now as NSString, "Token": ud.string(forKey: "deviceToken")! as NSString], withCompletionBlock: {(error,ref) in
         
         if let _ = error {
         print("errore")
         } else {
         //if any error doesn't occures, come back to root
         
         ud.set(true, forKey: "isBooked")
         let booking = Booking(codeActivity: self.codeActivity!, nameBooker: self.TFName.text!, numPeople: Int(self.manyPeople.text!)! , codeBooking: ref.key!)
         
         let encodedBooking : Data = NSKeyedArchiver.archivedData(withRootObject: booking)
         
         ud.set(encodedBooking, forKey: "Booking")
         
         self.navigationController?.popToRootViewController(animated: true)
         }
         
         })
         */
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func plusssbutton(_ sender: Any) {
        
        people += 1
        clientlabel.text = String(people)
       
    }
    
    @IBAction func minusssbbutton(_ sender: Any) {
        
        people -= 1
        clientlabel.text = String(people)
        
       
    }
}

extension AddCustomerViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}


