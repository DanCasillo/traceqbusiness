import UIKit
import Firebase

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var totalCountLabel: UILabel!
    @IBOutlet weak var segmentedTables: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var bookings : [Booking]!
    var ref : DatabaseReference!
    var bookingsToShow : [Booking]!
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addBooking(_:)), name: Notification.Name(rawValue: "bookingIsAdded"), object: nil)
        tableView.delegate = self
        tableView.dataSource = self
        
       
        let rangeString = segmentedTables.titleForSegment(at: 0)
        var rangeInt : [Int] = rangeString!.components(separatedBy: "-").map{Int($0)!}
        bookingsToShow = bookings.filter{$0.numPeople >= rangeInt[0] && $0.numPeople <= rangeInt[1]}
        totalCountLabel.text = "\(bookings.count)"
        // Do any additional setup after loading the view.
        title = "Customers"
        ref = Database.database().reference().child("Prenotazione").child(UserDefaults.standard.value(forKey: "codeActivity") as! String)
      /*  ref.observe(.childAdded, with: {snapshot in
            let key = snapshot.key
            let values = snapshot.value as! NSDictionary
            print("key : \(key) snap: \(values)")
            let booking = Booking(nameBooker: values["Nome"] as! String, numPeople: values["Numero"] as! Int, codeBooking: key, deviceBooking: values["Token"] as! String)
            self.bookings.append(booking)
//            print(booking.deviceBooking!)
        })*/
        
        
        
        
    }
    
    func deleteBooking(codeBooking : String){
        ref = Database.database().reference().child("Prenotazione").child(UserDefaults.standard.value(forKey: "codeActivity") as! String).child(codeBooking)
        
        ref.removeValue(completionBlock: {(error,ref) in
            if let error = error {
                print("DELETE ERROR: \(error)")
            } else {
                var count : Int = 0
                var trovato : Bool = false
                while (count < self.bookings.count && !trovato){
                    if self.bookings[count].codeBooking == codeBooking {
                        trovato = true
                    } else {
                        count += 1
                    }
                }
                
                self.bookings.remove(at: count)

                self.totalCountLabel.text = "\(self.bookings.count)"
            }
        })
    }
    
    @IBAction func changedTables(_ sender: UISegmentedControl) {
        let rangeString = sender.titleForSegment(at: sender.selectedSegmentIndex)
        
        if sender.selectedSegmentIndex == sender.numberOfSegments - 1 {
            let initialValue = rangeString?.components(separatedBy: "+")
            
            bookingsToShow = bookings.filter{$0.numPeople! >= Int(initialValue![0])!}
        } else {
            var rangeInt : [Int] = rangeString!.components(separatedBy: "-").map{Int($0)!}
            print("countBookings:\(bookings.count)")
                   bookingsToShow = bookings.filter{$0.numPeople >= rangeInt[0] && $0.numPeople <= rangeInt[1]}
        }
        print("countAfterChange : \(bookingsToShow.count)")
        tableView.reloadData()
        
    }
    
    @objc func addBooking(_ notification : Notification) {
        let booking = notification.userInfo!["Booking"] as! Booking
        bookings.append(booking)
        self.totalCountLabel.text = "\(bookings.count)"
              
        self.changedTables(segmentedTables)
   }
    
    public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let notification = notficationAction(at: indexPath)
        
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete, notification])
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    
    func notficationAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "notification") {(action, view, completion) in
            print(self.bookings[indexPath.section].deviceBooking!)
            self.bookings[indexPath.section].alertBooker()
            completion(true)
        }
        
        action.image = UIImage(named: "campanella")
        action.backgroundColor = .orange
        
        return action
    }
    
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
        // ////// qui dentro ci metti .destructive
        
        let action = UIContextualAction(style: .normal, title: "delete") {(action, view, completion) in
            ////per implementare la funzione qui sotto di delete ho bisgno di un index che mo viene fornito a database+gente messa manualmente
            //            self.tableView.deleteRows(at: [indexpath], with: .automatic)
            let indexSet = IndexSet(integer: indexPath.section)
            self.tableView.beginUpdates()
            self.tableView.deleteSections(indexSet, with: .right)
            
            self.deleteBooking(codeBooking: self.bookingsToShow[indexPath.section].codeBooking)
            
            self.bookingsToShow.remove(at: indexPath.section)
            self.tableView.endUpdates()
            completion(true)
            print("count:\(self.bookingsToShow.count)")
        }
        
        action.image = UIImage(named: "delete")
        
        action.backgroundColor = .red
        return action
    }
    
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return bookingsToShow.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        cell?.backgroundView?.backgroundColor = .orange
        
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        let cell = tableView.cellForRow(at: indexPath!)
        
        cell?.backgroundView?.backgroundColor = .clear
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! CustomTableViewCell
        
        cell.nameLabel.text = bookingsToShow[indexPath.section].nameBooker
        cell.numberlabel.text = "\(bookingsToShow[indexPath.section].numPeople!)"
        print(bookingsToShow[indexPath.section].numPeople!)
        cell.deviceToken = bookingsToShow[indexPath.section].deviceBooking
        
        cell.timelabel.text = "20min"
        
        cell.contentView.clipsToBounds = true
        
        cell.contentView.layer.cornerRadius = 10
        
        
        cell.backgroundView = UIView(frame: cell.bounds)
        cell.backgroundView?.layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
        cell.backgroundView?.clipsToBounds = true
        cell.backgroundView?.layer.cornerRadius = 10
        
        return cell
    }
    
    
    
    
    
}



