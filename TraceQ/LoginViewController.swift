//
//  LoginViewController.swift
//  TraceQ
//
//  Created by Francesco Aroldo on 05/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import MessageUI
import Firebase
import TransitionButton

class LoginViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var IDCode: UITextField!
    @IBOutlet weak var PASSWORD: UITextField!
    @IBOutlet weak var loginButton: TransitionButton!
    
    @IBOutlet weak var contUS: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGesture()
        IDCode.layer.borderWidth = 0
        IDCode.clipsToBounds = true
        IDCode.layer.cornerRadius = 13
        PASSWORD.clipsToBounds = true
        PASSWORD.layer.cornerRadius = 13
        PASSWORD.isSecureTextEntry = true
        loginButton.cornerRadius = 26
        loginButton.addTarget(self, action: #selector(performLogin(_:)), for: .touchUpInside)
    }
    
    private func tapGesture(){
        let tapGesturez = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapGesturez)
    }
    @objc func handleTap(){
        view.endEditing(true)
    }
    
    @IBAction func done1(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @objc func performLogin(_ sender: Any) {
        let button  = sender  as? TransitionButton
        button?.startAnimation()
        Auth.auth().signIn(withEmail: IDCode.text!, password: PASSWORD.text!, completion: {(authresult, error) in
            if error != nil {
                button?.stopAnimation(animationStyle: .shake, revertAfterDelay: 1, completion: {
                    print(self.PASSWORD.text)
                    print("error : \(error)")
                    let alert = UIAlertController(title: "ERROR!", message: "Invalid credentials", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {(action) in
                        print("dismiss")
                        
                    } )
                    
                    alert.addAction(cancelAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                })
                
            }
            else {
                let userID = authresult?.user.uid
                
                let docRef = Firestore.firestore()
                
                print(userID!)
                docRef.collection("Users").document(userID!).getDocument{(document,error) in
                    guard let document = document, document.exists else {print("Error fetching document :\(error!)")
                        return
                    }
                    
                    let codeActivity = document.data()!["Code"] as! String
                    print("CODICE = \(codeActivity)")
                    UserDefaults.standard.set(codeActivity, forKey: "codeActivity")
                    button?.stopAnimation(animationStyle: .expand, revertAfterDelay: 2, completion: {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "loadingVC") as! LoadingViewController
                        self.view.window?.rootViewController = vc
                    })
                    
                }
                
                
                print("Login riuscito")
            }
        })
    }
    
    @IBAction func sendMail(_ sender: UIButton) {
        let mailComposeViewController = mailController()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            mailError()        }
    }
    
    
    func mailController() -> MFMailComposeViewController{
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        
        mailComposer.setToRecipients(["traceq.service@gmail.com"])
        mailComposer.setSubject("Request ID CODE")
        mailComposer.setMessageBody("Insert your restaurant name and your Instagram contact so that we could check if it is requested by you", isHTML: false)
        
        return mailComposer
    }
    
    func mailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email!", message: "Your device could not send email now.", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
