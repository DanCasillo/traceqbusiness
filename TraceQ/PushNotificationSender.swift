import UIKit

class PushNotificationSender {
    
    let serverKey = "AAAA0JYfi1Q:APA91bGI-LApUY83J8SlwL1A0Mak4NnKLZrg5Qq0ar0s4R3QgYF7ZCK_ZFR5sbnfKYIByHF2Mm06Fg8vs_hN4jCwQX7D5HCaIdl2tvWj6A7XOMg5H1aur2mA4nocRblTjLnivRdMRrz4"
    
    func sendPushNotification(to token: String, title: String, body: String) {
        print("to : \(token)")
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body, "sound" : "default"],
                                           "data" : ["user" : "test_id"]
        ]

        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(serverKey)", forHTTPHeaderField: "Authorization")

        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
