//
//  Booking.swift
//  TraceQ
//
//  Created by Danilo Casillo on 15/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class Booking: NSObject {
    
    var nameBooker : String!
    var numPeople : Int!
    var codeBooking : String!
    var deviceBooking : String?
    
    init(nameBooker : String, numPeople: Int, codeBooking : String, deviceBooking: String?) {
        self.nameBooker = nameBooker
        self.numPeople = numPeople
        self.codeBooking = codeBooking
        self.deviceBooking = deviceBooking
    }
    
    func alertBooker() {
        let sender = PushNotificationSender()
        print(deviceBooking!)
        guard let deviceBooking = deviceBooking else {return}
        
        sender.sendPushNotification(to: deviceBooking, title: "Sei il prossimo", body: "Fra 10 minuti toccherà a te! Preparati ad entrare")
    }

}
