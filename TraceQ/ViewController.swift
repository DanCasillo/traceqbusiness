//
//  ViewController.swift
//  trySettings
//
//  Created by Francesco Aroldo on 18/05/2020.
//  Copyright © 2020 Francesco Aroldo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var less1: UIButton!
    @IBOutlet weak var sec1: UILabel!
    @IBOutlet weak var add1: UIButton!
    @IBOutlet weak var less11: UIButton!
    @IBOutlet weak var sec12: UILabel!
    @IBOutlet weak var add12: UIButton!
    @IBOutlet weak var less2: UIButton!
    @IBOutlet weak var sec2: UILabel!
    @IBOutlet weak var add2: UIButton!
    @IBOutlet weak var less21: UIButton!
    @IBOutlet weak var sec22: UILabel!
    @IBOutlet weak var add23: UIButton!
    @IBOutlet weak var less3: UIButton!
    @IBOutlet weak var sec3: UILabel!
    @IBOutlet weak var add3: UIButton!
    @IBOutlet weak var cont3: UIImageView!
    @IBOutlet weak var seg3: UIImageView!
    @IBOutlet weak var from3: UILabel!
    @IBOutlet weak var to3: UILabel!
    @IBOutlet weak var less31: UIButton!
    @IBOutlet weak var sec31: UILabel!
    @IBOutlet weak var cont31: UIImageView!
    @IBOutlet weak var add31: UIButton!
    @IBOutlet weak var addSeg: UIButton!
    
    var people: Int = 2
    var people1 = Int()
    var people2 = Int()
    var people3 = Int()
    var people4 = Int()
    var people5 = Int()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//          let sender = PushNotificationSender()
        //        sender.sendPushNotification(to: "eG-IXrcQ60c0sGTLEqshQS:APA91bE7OKqOaJ29lvXsbYwTIwFW-sDvrXFmHi8Sc77bYGM140lha9qVxl-oUz2PF-v-XHU_6ra_HCzwQjCwNyKzQsPHXX96uP5XPpyoXQRz0C7GXrIhBW7F3fTn3bHLcHp3Bq6QAoJt", title: "Prova", body: "sasa")
        
    
        people1 = people + 1
        people2 = people1 + 1
        people3 = people2 + 1
        people4 = people3 + 1
        people5 = people4 + 1
        
        sec1.text = "\(people)"
        sec12.text = "\(people1)"
        sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
        less11.isUserInteractionEnabled = false
        less2.isUserInteractionEnabled = false
        less21.isUserInteractionEnabled = false
        less3.isUserInteractionEnabled = false
        less31.isUserInteractionEnabled = false
    }

    @IBAction func addSeg3(_ sender: UIButton) {
        addSeg.isHidden = true
        less3.isHidden = false
        sec3.isHidden = false
        add3.isHidden = false
        cont3.isHidden = false
        seg3.isHidden = false
        from3.isHidden = false
        to3.isHidden = false
        less31.isHidden = false
        cont31.isHidden = false
        add31.isHidden = false
        sec31.isHidden = false
    }
    
    @IBAction func less1(_ sender: UIButton) {
        people -= 1
        people1 -= 1
        people2 -= 1
        people3 -= 1
        people4 -= 1
        people5 -= 1
        sec1.text = "\(people)"
        sec12.text = "\(people1)"
        sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
               
               
               if sec1.text == "1"{
                   less1.isUserInteractionEnabled = false
               }
    }
    
    @IBAction func add1(_ sender: UIButton) {
        less1.isUserInteractionEnabled = true
        people += 1
        people1 += 1
        people2 += 1
        people3 += 1
        people4 += 1
        people5 += 1
        sec1.text = "\(people)"
        sec12.text = "\(people1)"
        sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
    }
    
    @IBAction func less11(_ sender: UIButton) {
        people1 -= 1
        people2 -= 1
        people3 -= 1
        people4 -= 1
        people5 -= 1
        sec12.text = "\(people1)"
        sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
        if sec12.text == "\(people + 1)"{
            less11.isUserInteractionEnabled = false
        }
        
    }
    
    @IBAction func add11(_ sender: UIButton) {
        less11.isUserInteractionEnabled = true
        people1 += 1
        people2 += 1
        people3 += 1
        people4 += 1
        people5 += 1
        sec12.text = "\(people1)"
        sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
    }
    
    @IBAction func less2(_ sender: UIButton) {
        people2 -= 1
        people3 -= 1
        people4 -= 1
        people5 -= 1
        less21.isUserInteractionEnabled = true
               sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
               
               if sec2.text == "\(people1 + 1)"{
                   less2.isUserInteractionEnabled = false
               }
    }
    
    @IBAction func add2(_ sender: Any) {
        less2.isUserInteractionEnabled = true
        people2 += 1
        people3 += 1
        people4 += 1
        people5 += 1
        sec2.text = "\(people2)"
        sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
    }
    
    @IBAction func less21(_ sender: Any) {
        people3 -= 1
        people4 -= 1
        people5 -= 1
               sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
               if sec22.text == "\(people2 + 1)"{
                   less21.isUserInteractionEnabled = false
               }
    }
    
    @IBAction func add21(_ sender: Any) {
        less21.isUserInteractionEnabled = true
               people3 += 1
        people4 += 1
        people5 += 1
               sec22.text = "\(people3)"
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
    }
    
    @IBAction func less3(_ sender: UIButton) {
        people4 -= 1
        people5 -= 1
        less31.isUserInteractionEnabled = true
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
               
               if sec3.text == "\(people3 + 1)"{
                   less3.isUserInteractionEnabled = false
               }
    }
    
    @IBAction func add3(_ sender: UIButton) {
        less3.isUserInteractionEnabled = true
        people4 += 1
        people5 += 1
        sec3.text = "\(people4)"
        sec31.text = "\(people5)"
    }
    
    @IBAction func less31(_ sender: UIButton) {
               people5 -= 1
               sec31.text = "\(people5)"
                      if sec31.text == "\(people4 + 1)"{
                          less31.isUserInteractionEnabled = false
                      }
    }
    
    @IBAction func add31(_ sender: Any) {
        less31.isUserInteractionEnabled = true
        people5 += 1
        sec31.text = "\(people5)"
    }
}

