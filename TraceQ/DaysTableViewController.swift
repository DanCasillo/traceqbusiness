//
//  DaysTableViewController.swift
//  trySettings
//
//  Created by Francesco Aroldo on 22/05/2020.
//  Copyright © 2020 Francesco Aroldo. All rights reserved.
//

import UIKit

class day{
    var name: String
    var select : Bool
    var abbreviation: String
    
    init(name: String, select: Bool, abbreviation: String) {
        self.name = name
        self.select = select
        self.abbreviation = abbreviation;
    }
}

class DaysTableViewController: UITableViewController {
    
     var elementSelected: Int = 0
    let defaults = UserDefaults.standard
    
    

    var week = [
        day(name: "MONDAY", select: false, abbreviation: "M"),
        day(name: "TUESDAY", select: false, abbreviation: "TU"),
        day(name: "WEDNESDAY", select: false, abbreviation: "W"),
        day(name: "THURSDAY", select: false, abbreviation: "TH"),
        day(name: "FRIDAY", select: false, abbreviation: "F"),
        day(name: "SATURDAY", select: false, abbreviation: "SA"),
        day(name: "SUNDAY", select: false, abbreviation: "SU")
        
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.allowsMultipleSelection = true
        tableView.isScrollEnabled = false
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor(red: 255/255, green: 214/255, blue: 162/255, alpha: 1)
        
        let select1 = defaults.bool(forKey: "select")
        print(select1)
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if(defaults.bool(forKey: "select") ){
//            var daysSelected = [day]()
//
//            for day in week{
//                if day.select == true {
//                    daysSelected.append(day)
//                }
//            }
//
//            defaults.set(daysSelected, forKey: "daysSelected")
//        }
//    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return week.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let nameDays = week[indexPath.row]
        cell.textLabel?.text = nameDays.name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = .orange
        selectedCell.backgroundColor = .orange
        selectedCell.selectionStyle = .none
        selectedCell.accessoryType = .checkmark
        selectedCell.tintColor = .black
//        week[indexPath.row].select = true
        defaults.set(true, forKey: "select")
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = .white
        cellToDeSelect.accessoryType = .none
//        week[indexPath.row].select = false
        defaults.set(false, forKey: "select")
    }
//    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        self.elementSelected = indexPath.row
//
//        return indexPath
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let TableV = segue.destination as! TableViewController

        TableV.daysS.text = week[self.elementSelected].abbreviation
        TableV.daysS.textColor = .black
    }
    
     
}
