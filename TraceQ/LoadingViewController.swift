//
//  LoadingViewController.swift
//  TraceQ
//
//  Created by Danilo Casillo on 15/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import Firebase

class LoadingViewController: UIViewController {
    
    var ref : DatabaseReference!
    var bookings = [Booking]()
    var hasLoaded : Bool = false
    var images : [UIImage] = [UIImage(named: "Notepad-1")!, UIImage(named: "Notepad-2")!, UIImage(named: "Notepad-3")!]
    @IBOutlet weak var loadingNotes: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadingNotes.animationImages = images
        loadingNotes.animationDuration = 1.27
        loadingNotes.startAnimating()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let code = UserDefaults.standard.value(forKey: "codeActivity") as! String
        ref = Database.database().reference().child("Prenotazione")
        
        loadBookings(code) { isFinished in
            if (!self.hasLoaded){
                let navc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "rootViewController") as? UINavigationController
                let vc = navc?.children[0] as! HomeViewController
                vc.bookings = self.bookings
                let transition: CATransition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.fade
                transition.subtype = CATransitionSubtype.fromBottom
                self.view.window!.layer.add(transition, forKey: nil)
                self.dismiss(animated: false, completion: {
                    self.view.window?.rootViewController = navc
                    self.hasLoaded = true
                })}
            else
            {
                NotificationCenter.default.post(name: Notification.Name("bookingIsAdded"), object: nil, userInfo: ["Booking" : self.bookings.last])
            }
            
            
        }
    }
    
    func loadBookings(_ code: String, completion: @escaping (Bool) -> Void) {
        
        getCount(code, completion: {(isFinished,numberOfChild) in
            if (numberOfChild == 0) {
                completion(true)
            } else {
                var count : Int = 0
                self.ref.child(code).observe(.childAdded, with: {snapshot in
                    count += 1
                    print(snapshot)
                    let key = snapshot.key
                    let values = snapshot.value as! NSDictionary
                    
                    let booking = Booking(nameBooker: values["Nome"] as! String, numPeople: values["Numero"] as! Int, codeBooking: key, deviceBooking: values["Token"] as! String)
                    self.bookings.append(booking)
                    
                    if !self.hasLoaded {
                        if count == numberOfChild {
                            print(count)
                            completion(true)
                        }
                    } else {
                        completion(true)
                    }
                    
                    
                    /* for child in snapshot.children {
                     let child = child as! DataSnapshot
                     print(child)
                     let values = child.value as! NSDictionary
                     
                     print(booking.deviceBooking!)
                     }*/
                    
                })
            }
        })
        
    }
    
    func getCount(_ code : String, completion: @escaping (Bool, Int) -> Void)  {
        var count : Int = 0
        var numberOfChild = 0
        ref.child(code).observeSingleEvent(of: .value, with: {snapshot in
            print("\(snapshot.key) has \(snapshot.childrenCount) ")
            numberOfChild = Int(snapshot.childrenCount)
            completion(true,numberOfChild)
        })
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
