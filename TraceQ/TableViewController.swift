//
//  TableViewController.swift
//  trySettings
//
//  Created by Francesco Aroldo on 18/05/2020.
//  Copyright © 2020 Francesco Aroldo. All rights reserved.
//

import UIKit
import Firebase

class TableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    var pickerView = UIPickerView()
    var openHours = ["10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "00:00", "01:00", "02:00"]
    var openHours1 = [String]()
    @IBOutlet weak var TFOpenH: UITextField!
    @IBOutlet weak var TF2: UITextField!
    @IBOutlet weak var switchOnline: UISwitch!
    @IBOutlet weak var selectD: UIButton!
    @IBOutlet weak var daysS: UILabel!
    @IBOutlet weak var GL: UITextField!
    

    var itemSelected = ""
    let sectionHeaderTitleArray = ["ACCOUNT","BOOKINGS","CUSTOMIZATION"]
 
    var switchIsChanged : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isScrollEnabled = false
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        let tapGesturez = UITapGestureRecognizer(target: self, action: #selector(handleTap))
               view.addGestureRecognizer(tapGesturez)
       
        GL.delegate = self
        
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self

        TFOpenH.delegate = self
        TF2.delegate = self
        
        TFOpenH.inputView = pickerView
        TF2.inputView = pickerView

        TF2.isUserInteractionEnabled = false
        self.pickerView = pickerView
        
        getType(completion: {type in
            let ref = Database.database().reference().child("Attività").child(type).child(UserDefaults.standard.value(forKey: "codeActivity") as! String)
            ref.observeSingleEvent(of: .value, with: {snap in
                print(snap.key)
                print(snap.value)
            })
            
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if switchIsChanged {
            
            getType(){ type in
                let ref = Database.database().reference().child("Attività").child(type).child(UserDefaults.standard.value(forKey: "codeActivity") as! String)
                print(ref)
                ref.child("Online").setValue(self.switchOnline.isOn)
                print(ref)
            }
        }
        
    }
    
    func getType(completion : @escaping (String) -> Void) {
        let ref = Database.database().reference().child("Attività")
        var trovato : Bool = false
        ref.observeSingleEvent(of: .value, with: {snapshot in
            for child in snapshot.children {
                let child = child as! DataSnapshot
                for child in child.children
                {
                    let child = child as! DataSnapshot
                    if child.key == UserDefaults.standard.value(forKey: "codeActivity") as! String {
                        trovato = true
                    } else {
                        trovato = false
                    }
                }
                if (trovato) {
                    completion(child.key)
                }
                
            }
           
        })
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if TFOpenH.isFirstResponder{
            TF2.text = ""
            return openHours.count
        } else if TF2.isEditing{
            openHours1 = openHours.filter({ $0 > TFOpenH.text!})
            openHours1.append(contentsOf: ["00:00", "01:00", "02:00"])
            return openHours1.count
        }
        return 0
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if TFOpenH.isFirstResponder{
            return openHours[row]
        } else if TF2.isFirstResponder{
            return openHours1[row]
        }
        return nil
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let itemselected = openHours[row]
        if TFOpenH.isFirstResponder{
            TFOpenH.text = itemselected
            TF2.isUserInteractionEnabled = true
        } else if TF2.isEditing{
            let itemselected1 = openHours1[row]
            TF2.text = itemselected1
        }
    }
    
       @objc func handleTap(){
           view.endEditing(true)
       }
    
    @IBAction func switchStatus(_ sender: Any) {
        if switchOnline.isOn {
           print("Switch is ON")
        } else {
            print("Switch is OFF")
        }
        switchIsChanged = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        let updateText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updateText.count < 3
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 5
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        
        let label = UILabel(frame: CGRect(x: 10, y: 3, width: view.frame.size.width, height: 25))
        label.text = self.sectionHeaderTitleArray[section]
        label.textColor = .black
        returnedView.addSubview(label)

        return returnedView
    }
}
