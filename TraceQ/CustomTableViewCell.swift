//
//  CustomTableViewCell.swift
//  TraceQGestore
//
//  Created by Simone Di Guida on 08/06/2020.
//  Copyright © 2020 Simone Di Guida. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberlabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    
    var deviceToken : String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
